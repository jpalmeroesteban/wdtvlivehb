﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Toolkit.Library;
using TvdbLib.Data;
using WDTVHubGen.LoggingService;

namespace WDTVHubGen
{


    /*  public class MovieItem
      {
          private string cleanFileName;
          private string uncleanFileName;
          private string description;
          private string fullFileName;
          private bool thumbnailPresent;
          private string title;
          private bool xmlPresent;
          private bool subsPresent;
          private string thumbPath;
          private DateTime processedDate;
          private details movieDetail;
          private bool vobDirectory;

          public bool VOBDirectory { get { return vobDirectory; } set { vobDirectory = value; } }

          public DateTime ProcessedDate
          {
              get { return processedDate; }
              set { processedDate = value; }
          }

          public bool SubsPresent
          { get { return subsPresent; } set { subsPresent = value; } }

          public details MovieDetail
          {
              get { return movieDetail; }
              set { 
                      movieDetail = value;
                      Description = movieDetail.overview;
                      Title = movieDetail.title;
                  }
          }

          public string ThumbPath
          {
              get { return thumbPath; }
              set { thumbPath = value; }
          }

          public string UncleanFileName
          {
              get { return uncleanFileName; }
              set { uncleanFileName = value; }
          }

          public string CleanFileName
          {
              get { return cleanFileName; }
              set { cleanFileName = value; }
          }

          public string Description
          {
              get { return description; }
              set { description = value; }
          }

          public string FullFileName
          {
              get { return fullFileName; }
              set { fullFileName = value; }
          }

          public bool ThumbnailPresent
          {
              get { return thumbnailPresent; }
              set { thumbnailPresent = value; }
          }

          public string Title
          {
              get { return title; }
              set { title = value; }
          }

          public bool XmlPresent
          {
              get { return xmlPresent; }
              set { xmlPresent = value; }
          }

       

          public MovieItem(string cleanFileName, string description, string fullFileName, string thumbpath, bool xmlPresent, bool thumbnailPresent, bool subspresent, string title, details movieDetail)
          {
              this.cleanFileName = cleanFileName;
            
              this.description = description;
              this.fullFileName = fullFileName;
              if (Path.GetFileName(this.fullFileName) == "VTS_01_0.VOB") 
              {
                  this.vobDirectory = true;
                  // clean file name is equal to the directory above...
                  DirectoryInfo di = Directory.GetParent(this.fullFileName);
                  this.cleanFileName = di.FullName;
              } else
                  this.vobDirectory = false;
              this.thumbPath = thumbpath;
              this.thumbnailPresent = thumbnailPresent;
              this.subsPresent = subspresent;
              this.title = title;
              this.xmlPresent = xmlPresent;
              if (this.xmlPresent) this.movieDetail = movieDetail;  else  this.movieDetail = null;
              UpdateProcessedDate();
            
          }

          public MovieItem(string cleanFileName, string fullFileName, bool thumbnailPresent, bool xmlPresent, bool subspresent)
          {
              this.cleanFileName = cleanFileName;
              this.fullFileName = fullFileName;
              if (Path.GetFileName(this.fullFileName) == "VTS_01_0.VOB")
              {
                  this.vobDirectory = true;
                  // clean file name is equal to the directory above...
                  DirectoryInfo di = Directory.GetParent(this.fullFileName);
                  this.cleanFileName = Path.GetFileName(di.FullName);
              }
              else
                  this.vobDirectory = false;
              this.title = cleanFileName;
              this.uncleanFileName = fullFileName;
              this.thumbnailPresent = thumbnailPresent;
              this.subsPresent = subspresent;
              this.xmlPresent = xmlPresent;
              if (!this.thumbnailPresent) this.thumbPath = "notfound.jpg";
              if (!this.xmlPresent)
              {
                  this.description = "File Not Processed: " + this.cleanFileName;
              }
              UpdateProcessedDate();
            
          }

          public void UpdateProcessedDate()
          {
              if (!this.xmlPresent)
              {
                  this.processedDate = File.GetCreationTime(this.fullFileName);
              }
              else
              {
                  this.processedDate = File.GetCreationTime(Path.ChangeExtension(this.fullFileName, "xml"));
              }
          }
      }

      public class TVItem
      {
          private string cleanFileName;
          private string uncleanFileName;
          private string description;
          private string fullFileName;
          private bool thumbnailPresent;
          private string title;
          private bool xmlPresent;
          private string thumbPath;
          private details tvMovieDetail;
          private int season;
          private int episode;
          private string seriesname;
          private string episodeName;
          private bool subsPresent;
          private DateTime processedDate;

          public DateTime ProcessedDate
          {
              get { return processedDate; }
              set { processedDate = value; }
          }
          public bool SubsPresent { get { return subsPresent; } set { subsPresent = value; } }

          public string SeriesName { get { return seriesname; }
              set { seriesname = value; }
          }

          public string EpisodeName { get { return episodeName; } set { episodeName = value; } }

          public int Season
          {
              get { return season; }
              set { season = value; }
          }

          public int Episode
          {
              get { return episode; }
              set { episode = value; }
          }

          public details MovieDetail
          {
              get { return tvMovieDetail; }
              set
              {
                  tvMovieDetail = value;
                  Description = tvMovieDetail.overview;
                  Title = tvMovieDetail.title;
              }
          }

          public string ThumbPath
          {
              get { return thumbPath; }
              set { thumbPath = value; }
          }

          public string UncleanFileName
          {
              get { return uncleanFileName; }
              set { uncleanFileName = value; }
          }

          public string CleanFileName
          {
              get { return cleanFileName; }
              set { cleanFileName = value; }
          }

          public string Description
          {
              get { return description; }
              set { description = value; }
          }

          public string FullFileName
          {
              get { return fullFileName; }
              set { fullFileName = value; }
          }

          public bool ThumbnailPresent
          {
              get { return thumbnailPresent; }
              set { thumbnailPresent = value; }
          }

          public string Title
          {
              get { return title; }
              set { title = value; }
          }

          public bool XmlPresent
          {
              get { return xmlPresent; }
              set { xmlPresent = value; }
          }

          public TVItem(string cleanFileName, string description, string fullFileName, int season, int episode, string thumbpath, bool xmlPresent, bool thumbnailPresent,bool subspresent, string title, details movieDetail)
          {
              this.cleanFileName = cleanFileName;

              this.description = description;
              this.fullFileName = fullFileName;
              this.thumbPath = thumbpath;
              //this.seriesname = seriesName;
              //this.episodeName = episodename;
              this.thumbnailPresent = thumbnailPresent;
              this.subsPresent = subspresent;
              this.title = title;
              this.xmlPresent = xmlPresent;
              this.episode = episode;
              this.season = season;
              if (this.xmlPresent) this.tvMovieDetail = movieDetail; else this.tvMovieDetail = null;
              UpdateProcessedDate();
          }

          public TVItem(string cleanFileName, string fullFileName, int season, int episode, bool thumbnailPresent, bool xmlPresent, bool subspresent)
          {
              this.cleanFileName = cleanFileName;
              this.fullFileName = fullFileName;
              this.uncleanFileName = fullFileName;
              this.thumbnailPresent = thumbnailPresent;
              this.subsPresent = subspresent;
              this.season = season;
              this.episode = episode;
            

              this.xmlPresent = xmlPresent;
              if (!this.thumbnailPresent) this.thumbPath = "notfound.jpg";
              if (!this.xmlPresent)
              {
                  this.description = "File Note Processed: " + this.cleanFileName;
              }
              UpdateProcessedDate();
          }

          public string GetShowName()
          {
              // this needs to be really really really fucking smart... I will have to use regex to Parse out the show title which should always 

              string showname = this.title;
              showname = RemoveEpisodeAndBeyondInformation(showname);

              if (showname.Trim()==string.Empty) 
              {
                  // find alternative naming source, probably the directory name...
                  string[] parts = fullFileName.Split(Path.DirectorySeparatorChar);

              }
              return showname;
          }

          private string RemoveEpisodeAndBeyondInformation(string cleanfilename)
          {
           
              var rx = new Regex(@"(S(?<season>\d{1,2})E(?<episode>\d{2}))|(\[(?<season>\d{1,2})x(?<episode>\d{2})\])",
                                 RegexOptions.IgnoreCase);

              MatchCollection matches = rx.Matches(cleanfilename);
              if (matches.Count > 0)
              {
                  cleanfilename = cleanfilename.Substring(0, matches[0].Index);
                  //cleanfilename = rx.Replace(cleanfilename," ");
                  //return rx.Replace(cleanfilename, " ");
                  return cleanfilename;
              }
              var rx2 = new Regex(@"(?<combined>\d{3,4})", RegexOptions.IgnoreCase);
              MatchCollection matches2 = rx2.Matches(cleanfilename);
              if (matches2.Count > 0)
              {
                  cleanfilename = cleanfilename.Substring(0, matches2[0].Index);
                  return cleanfilename;
              }
              //return rx2.Replace(cleanfilename, " ");
              // else
              var rx3 = new Regex(@"(?<season>\d{1,2})x(?<episode>\d{2})", RegexOptions.IgnoreCase);
              MatchCollection matches3 = rx3.Matches(cleanfilename);
              if (matches3.Count > 0)
              {
                  cleanfilename = cleanfilename.Substring(0, matches3[0].Index);
                  return cleanfilename;
              }

              return cleanfilename;
          }
          public void UpdateProcessedDate()
          {
              if (!this.xmlPresent)
              {
                  this.processedDate = File.GetCreationTime(this.fullFileName);
              }
              else
              {
                  this.processedDate = File.GetCreationTime(Path.ChangeExtension(this.fullFileName, "xml"));
              }
          }
      }
  }
      */

}