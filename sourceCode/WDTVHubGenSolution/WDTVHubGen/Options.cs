using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace Toolkit.Library
{
    /// <summary>
    /// Options is a class that allows the abstraction of the app.config file with full save capability, exposes an object to the user/main program
    /// and allows for easy configuration.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// LoggingLevels are for full logging of everying (verbose), only exception handling logging (errors) and no logging (none) (default)
        /// </summary>
        public enum LoggingLevels 
        {
            none,errors,verbose
        }
        public Options()
        {
            movieDirectory = MovieDirectory;
            tVShowDirectory = TVShowDirectory;
            validExtensions = ValidExtensions;
            removeWords = RemoveWords;
            verboseErrors = VerboseErrors;
            aggressiveWords = AggressiveWords;
            seriesFolderLocation = SeriesFolderLocation;
            recursiveSearch = RecursiveSearch;
            substitutions = Substitutions;
            descriptionTemplate = DescriptionTemplate;
            tVTitleTemplate = TVTitleTemplate;
            movieTitleTemplate = MovieTitleTemplate;
            tVFileTemplate = TVFileTemplate;
            movieFileTemplate = MovieFileTemplate;
            ignoreTVOnMovies = IgnoreTVOnMovies;
            languageToReturn = LanguageToReturn;
            numberOnShow = NumberOnShow;
            savePictureWithMetadata = SavePictureWithMetadata;
            suppressVOBProcessing = SuppressVOBProcessing;
            autoAggressiveLookup = AutoAggressiveLookup;
            runTimeFormat = RunTimeFormat;
            showCards = ShowCards;
            saveBackdrops = SaveBackdrops;
            useOriginalFilenames = UseOriginalFilenames;
            // this must be the last line.
            LoadingFirstTime = false;
            
        }

        private bool LoadingFirstTime = true;

        private string languageToReturn = string.Empty;
        private string substitutions = string.Empty;
        private string movieDirectory=string.Empty;
        private string tVShowDirectory = string.Empty;
        private string validExtensions = string.Empty;
        private string removeWords = string.Empty;
        private LoggingLevels verboseErrors;
        private int aggressiveWords;
        private string seriesFolderLocation = string.Empty;
        private string runTimeFormat = string.Empty;
        private string tVTitleTemplate = string.Empty;
        private string movieTitleTemplate = string.Empty;
        private string tVFileTemplate = string.Empty;
        private string movieFileTemplate = string.Empty;
        private string descriptionTemplate = string.Empty;
        private bool recursiveSearch;
        private bool ignoreTVOnMovies;
        private bool numberOnShow;
        private bool savePictureWithMetadata;
        private bool suppressVOBProcessing;
        private bool showCards;
        private bool saveBackdrops;
        private bool autoAggressiveLookup;
        private bool useOriginalFilenames;


        public string RunTimeFormat
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["RunTimeFormat"] == null)
                    {
                        throw (new ConfigurationException("RunTimeFormat String not in App.Config"));
                    }
                    else
                    {
                        runTimeFormat = config.AppSettings.Settings["RunTimeFormat"].Value.ToString();
                    }
                }
                return runTimeFormat;
            }
            set { runTimeFormat = value; }
        }

        //

        public string MovieFileTemplate
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["MovieFileTemplate"] == null)
                    {
                        throw (new ConfigurationException("MovieFileTemplate String not in App.Config"));
                    }
                    else
                    {
                        movieFileTemplate = config.AppSettings.Settings["MovieFileTemplate"].Value.ToString();
                    }
                }
                return movieFileTemplate;
            }
            set { movieFileTemplate = value; }
        }


        public string DescriptionTemplate
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["DescriptionTemplate"] == null)
                    {
                        throw (new ConfigurationException("DescriptionTemplate String not in App.Config"));
                    }
                    else
                    {
                        descriptionTemplate = config.AppSettings.Settings["DescriptionTemplate"].Value.ToString();
                    }
                }
                return descriptionTemplate;
            }
            set { descriptionTemplate = value; }
        }

        public string TVFileTemplate
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["TVFileTemplate"] == null)
                    {
                        throw (new ConfigurationException("TVFileTemplate String not in App.Config"));
                    }
                    else
                    {
                        tVFileTemplate = config.AppSettings.Settings["TVFileTemplate"].Value.ToString();
                    }
                }
                return tVFileTemplate;
            }
            set { tVFileTemplate = value; }
        }

        public string MovieTitleTemplate
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["MovieTitleTemplate"] == null)
                    {
                        throw (new ConfigurationException("MovieTitleTemplate String not in App.Config"));
                    }
                    else
                    {
                        movieTitleTemplate = config.AppSettings.Settings["MovieTitleTemplate"].Value.ToString();
                    }
                }
                return movieTitleTemplate;
            }
            set { movieTitleTemplate = value; }
        }

       public string TVTitleTemplate
       {
           get
           {
               if (LoadingFirstTime)
               {
                   Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                   if (config.AppSettings.Settings["TVTitleTemplate"] == null)
                   {
                       throw (new ConfigurationException("TVTitleTemplate String not in App.Config"));
                   }
                   else
                   {
                       tVTitleTemplate = config.AppSettings.Settings["TVTitleTemplate"].Value.ToString();
                   }
               }
               return tVTitleTemplate;
           }
           set { tVTitleTemplate = value; }
       }

        public string SeriesFolderLocation
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["SeriesFolderLocation"] == null)
                    {
                        throw (new ConfigurationException("SeriesFolderLocation String not in App.Config"));
                    }
                    else
                    {
                        seriesFolderLocation = config.AppSettings.Settings["SeriesFolderLocation"].Value.ToString();
                    }
                }
                return seriesFolderLocation;
            }
            set { seriesFolderLocation = value; }
        }


        public string MovieDirectory
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["MovieDirectory"] == null)
                    {
                        throw (new ConfigurationException("Movie Directory String not in App.Config"));
                    }
                    else
                    {
                        movieDirectory = config.AppSettings.Settings["MovieDirectory"].Value.ToString();
                    }
                }
                return movieDirectory;
            }
            set { movieDirectory = value; }
        }

        public string TVShowDirectory
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["TVShowDirectory"] == null)
                    {
                        throw (new ConfigurationException("TVShowDirectory String not in App.Config"));
                    }
                    else
                    {
                        tVShowDirectory = config.AppSettings.Settings["TVShowDirectory"].Value.ToString();
                    }
                }
                return tVShowDirectory;
            }
            set { tVShowDirectory = value; }
        }

        public string LanguageToReturn
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["LanguageToReturn"] == null)
                    {
                        throw (new ConfigurationException("LanguageToReturn String not in App.Config"));
                    }
                    else
                    {
                        languageToReturn = config.AppSettings.Settings["LanguageToReturn"].Value.ToString();
                    }
                }
                return languageToReturn;
            }
            set { languageToReturn = value; }
        }

        public string Substitutions
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["Substitutions"] == null)
                    {
                        throw (new ConfigurationException("Substitutions String not in App.Config"));
                    }
                    else
                    {
                        substitutions = config.AppSettings.Settings["Substitutions"].Value.ToString();
                    }
                }
                return substitutions;
            }
            set { substitutions = value; }
        }

        public string ValidExtensions
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["ValidExtensions"] == null)
                    {
                        throw (new ConfigurationException("ValidExtensions String not in App.Config"));
                    }
                    else
                    {
                        validExtensions = config.AppSettings.Settings["ValidExtensions"].Value.ToString();
                    }
                }
                return validExtensions;
            }
            set { validExtensions = value; }
        }



        public bool SaveBackdrops
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["SaveBackdrops"] == null)
                    {
                        throw (new ConfigurationException("SaveBackdrops bool not in App.Config"));
                    }
                    else
                    {
                        saveBackdrops = config.AppSettings.Settings["SaveBackdrops"].Value == "true";
                    }
                }
                return saveBackdrops;
            }
            set { saveBackdrops = value; }
        }


        //<add key="Automatic Aggressive Lookup" value="true"/>

        public bool AutoAggressiveLookup
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["AutoAggressiveLookup"] == null)
                    {
                        throw (new ConfigurationException("AutoAggressiveLookup bool not in App.Config"));
                    }
                    else
                    {
                        autoAggressiveLookup = config.AppSettings.Settings["AutoAggressiveLookup"].Value == "true";
                    }
                }
                return autoAggressiveLookup;
            }
            set { autoAggressiveLookup = value; }
        }

        public bool UseOriginalFilenames
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["UseOriginalFilenames"] == null)
                    {
                        throw (new ConfigurationException("UseOriginalFilenames bool not in App.Config"));
                    }
                    else
                    {
                        useOriginalFilenames = config.AppSettings.Settings["UseOriginalFilenames"].Value == "true";
                    }
                }
                return useOriginalFilenames;
            }
            set { useOriginalFilenames = value; }
        }

        public bool ShowCards
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["ShowCards"] == null)
                    {
                        throw (new ConfigurationException("ShowCards bool not in App.Config"));
                    }
                    else
                    {
                        showCards = config.AppSettings.Settings["ShowCards"].Value == "true";
                    }
                }
                return showCards;
            }
            set { showCards = value; }
        }

        public bool NumberOnShow
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["NumberOnShow"] == null)
                    {
                        throw (new ConfigurationException("NumberOnShow bool not in App.Config"));
                    }
                    else
                    {
                        numberOnShow = config.AppSettings.Settings["NumberOnShow"].Value == "true";
                    }
                }
                return numberOnShow;
            }
            set { numberOnShow = value; }
        }
        public bool SavePictureWithMetadata
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["SavePictureWithMetadata"] == null)
                    {
                        throw (new ConfigurationException("SavePictureWithMetadata bool not in App.Config"));
                    }
                    else
                    {
                        savePictureWithMetadata = config.AppSettings.Settings["SavePictureWithMetadata"].Value == "true";
                    }
                }
                return savePictureWithMetadata;
            }
            set { savePictureWithMetadata = value; }
        }

        public bool SuppressVOBProcessing
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["SuppressVOBProcessing"] == null)
                    {
                        throw (new ConfigurationException("SuppressVOBProcessing bool not in App.Config"));
                    }
                    else
                    {
                        suppressVOBProcessing = config.AppSettings.Settings["SuppressVOBProcessing"].Value == "true";
                    }
                }
                return suppressVOBProcessing;
            }
            set { suppressVOBProcessing = value; }
        }

        
        
        public bool RecursiveSearch
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["RecursiveSearch"] == null)
                    {
                        throw (new ConfigurationException("RecursiveSearch bool not in App.Config"));
                    }
                    else
                    {
                        recursiveSearch = config.AppSettings.Settings["RecursiveSearch"].Value == "true";
                    }
                }
                return recursiveSearch;
            }
            set { recursiveSearch = value; }
        }
         public bool IgnoreTVOnMovies
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["IgnoreTVOnMovies"] == null)
                    {
                        throw (new ConfigurationException("IgnoreTVOnMovies bool not in App.Config"));
                    }
                    else
                    {
                        ignoreTVOnMovies = config.AppSettings.Settings["IgnoreTVOnMovies"].Value == "true";
                    }
                }
                return ignoreTVOnMovies;
            }
            set { ignoreTVOnMovies = value; }
        }
        

       public LoggingLevels VerboseErrors
       {
           get
           {
               if (LoadingFirstTime)
               {
                   Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                   if (config.AppSettings.Settings["VerboseErrors"] == null)
                   {
                       throw (new ConfigurationException("VerboseErrors bool not in App.Config"));
                   }
                   else
                   {
                       switch (config.AppSettings.Settings["VerboseErrors"].Value)
                       {
                           case "none":
                               verboseErrors = LoggingLevels.none;
                               break;
                           case "errors":
                               verboseErrors = LoggingLevels.errors;
                               break;
                           case "verbose":
                               verboseErrors = LoggingLevels.verbose;
                               break;
                           default:
                               throw new ArgumentOutOfRangeException();
                       }
                       
                   }
               }
               return verboseErrors;
           }
           set { verboseErrors = value; }
       }
        

        public int AggressiveWords
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["AGgressiveWords"] == null)
                    {
                        throw (new ConfigurationException("AGgressiveWords not in App.Config"));
                    }
                    else
                    {
                        aggressiveWords = Convert.ToInt32(config.AppSettings.Settings["AggressiveWords"].Value);
                    }
                }
                return aggressiveWords;
            }
            set { aggressiveWords = value; }
        }

        public string RemoveWords
        {
            get
            {
                if (LoadingFirstTime)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (config.AppSettings.Settings["RemoveWords"] == null)
                    {
                        throw (new ConfigurationException("RemoveWords not in App.Config"));
                    }
                    else
                    {
                        removeWords = config.AppSettings.Settings["RemoveWords"].Value.ToString();
                    }
                }
                return removeWords;
            }
            set { removeWords = value; }
        }

        public void SaveSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.AppSettings.Settings["MovieDirectory"].Value = movieDirectory;
            config.AppSettings.Settings["TVShowDirectory"].Value = tVShowDirectory;
            config.AppSettings.Settings["ValidExtensions"].Value = validExtensions;
            config.AppSettings.Settings["RemoveWords"].Value = removeWords;
            config.AppSettings.Settings["Substitutions"].Value = substitutions;
            config.AppSettings.Settings["AggressiveWords"].Value = aggressiveWords.ToString();
            config.AppSettings.Settings["SeriesFolderLocation"].Value = seriesFolderLocation;
            config.AppSettings.Settings["RunTimeFormat"].Value = runTimeFormat;
            config.AppSettings.Settings["TVTitleTemplate"].Value = tVTitleTemplate;
            config.AppSettings.Settings["MovieTitleTemplate"].Value = movieTitleTemplate;
            config.AppSettings.Settings["TVFileTemplate"].Value = tVFileTemplate;
            config.AppSettings.Settings["MovieFileTemplate"].Value = movieFileTemplate;
            config.AppSettings.Settings["LanguageToReturn"].Value = languageToReturn;
            
            //config.AppSettings.Settings["DCSWorkingDirectory"].Value = dcsworkingdirectory;
            config.AppSettings.Settings["IgnoreTVOnMovies"].Value = ignoreTVOnMovies ? "true" : "false";
            config.AppSettings.Settings["VerboseErrors"].Value = verboseErrors.ToString();
            config.AppSettings.Settings["RecursiveSearch"].Value = recursiveSearch ? "true" : "false";
            config.AppSettings.Settings["NumberOnShow"].Value = numberOnShow ? "true" : "false";
            config.AppSettings.Settings["SavePictureWithMetadata"].Value = savePictureWithMetadata ? "true" : "false";
            config.AppSettings.Settings["SuppressVOBProcessing"].Value = suppressVOBProcessing ? "true" : "false";
            config.AppSettings.Settings["ShowCards"].Value = showCards ? "true" : "false";
            config.AppSettings.Settings["SaveBackdrops"].Value = saveBackdrops ? "true" : "false";
            config.AppSettings.Settings["UseOriginalFilenames"].Value = useOriginalFilenames ? "true" : "false";
            
            
            
            config.Save(ConfigurationSaveMode.Full);
            // Force a reload of the changed section.
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}       