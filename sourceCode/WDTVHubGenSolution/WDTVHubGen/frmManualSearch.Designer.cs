﻿namespace WDTVHubGen
{
    partial class frmManualSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManualSearch));
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.rdoMovie = new System.Windows.Forms.RadioButton();
            this.rdoTVSeries = new System.Windows.Forms.RadioButton();
            this.btnManualSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSeason = new System.Windows.Forms.TextBox();
            this.txtEpisode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIMDBNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(55, 13);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(384, 20);
            this.txtTitle.TabIndex = 1;
            // 
            // rdoMovie
            // 
            this.rdoMovie.AutoSize = true;
            this.rdoMovie.Checked = true;
            this.rdoMovie.Location = new System.Drawing.Point(55, 50);
            this.rdoMovie.Name = "rdoMovie";
            this.rdoMovie.Size = new System.Drawing.Size(91, 17);
            this.rdoMovie.TabIndex = 2;
            this.rdoMovie.TabStop = true;
            this.rdoMovie.Text = "Movie Search";
            this.rdoMovie.UseVisualStyleBackColor = true;
            // 
            // rdoTVSeries
            // 
            this.rdoTVSeries.AutoSize = true;
            this.rdoTVSeries.Location = new System.Drawing.Point(55, 73);
            this.rdoTVSeries.Name = "rdoTVSeries";
            this.rdoTVSeries.Size = new System.Drawing.Size(108, 17);
            this.rdoTVSeries.TabIndex = 3;
            this.rdoTVSeries.Text = "TV Series Search";
            this.rdoTVSeries.UseVisualStyleBackColor = true;
            // 
            // btnManualSearch
            // 
            this.btnManualSearch.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnManualSearch.Location = new System.Drawing.Point(445, 12);
            this.btnManualSearch.Name = "btnManualSearch";
            this.btnManualSearch.Size = new System.Drawing.Size(75, 23);
            this.btnManualSearch.TabIndex = 4;
            this.btnManualSearch.Text = "Search";
            this.btnManualSearch.UseVisualStyleBackColor = true;
            this.btnManualSearch.Click += new System.EventHandler(this.btnManualSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Season";
            // 
            // txtSeason
            // 
            this.txtSeason.Location = new System.Drawing.Point(250, 87);
            this.txtSeason.Name = "txtSeason";
            this.txtSeason.Size = new System.Drawing.Size(100, 20);
            this.txtSeason.TabIndex = 6;
            // 
            // txtEpisode
            // 
            this.txtEpisode.Location = new System.Drawing.Point(250, 113);
            this.txtEpisode.Name = "txtEpisode";
            this.txtEpisode.Size = new System.Drawing.Size(100, 20);
            this.txtEpisode.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Episode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(200, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "IMDB #";
            // 
            // txtIMDBNumber
            // 
            this.balloonTip1.SetBalloonCaption(this.txtIMDBNumber, "IMDB #");
            this.balloonTip1.SetBalloonText(this.txtIMDBNumber, "This needs to be the full imdb number. for example for 300 (the movie) the IMDB n" +
                    "umber is tt0416449");
            this.txtIMDBNumber.Location = new System.Drawing.Point(250, 48);
            this.txtIMDBNumber.Name = "txtIMDBNumber";
            this.txtIMDBNumber.Size = new System.Drawing.Size(100, 20);
            this.txtIMDBNumber.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(353, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 26);
            this.label5.TabIndex = 11;
            this.label5.Text = "putting a value here overrides\r\nthe movie name";
            // 
            // frmManualSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 143);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIMDBNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEpisode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSeason);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnManualSearch);
            this.Controls.Add(this.rdoTVSeries);
            this.Controls.Add(this.rdoMovie);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmManualSearch";
            this.Text = "Manual Title Search";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnManualSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtTitle;
        public System.Windows.Forms.RadioButton rdoMovie;
        public System.Windows.Forms.RadioButton rdoTVSeries;
        public System.Windows.Forms.TextBox txtSeason;
        public System.Windows.Forms.TextBox txtEpisode;
        public System.Windows.Forms.TextBox txtIMDBNumber;
        private DevComponents.DotNetBar.BalloonTip balloonTip1;
    }
}