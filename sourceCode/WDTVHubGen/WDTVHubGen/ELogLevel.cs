﻿namespace WDTVHubGen
{
    namespace LoggingService
    {

        public enum ELogLevel
        {

            DEBUG = 1,

            ERROR,

            FATAL,

            INFO,

            WARN

        }

    }
}
