﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Diagnostics;
using Toolkit.Library;
using WatTmdb.V3;

namespace WDTVHubGen
{
  public partial class MovieSearchResultForm : Form
  {
    //List<TvdbSearchResult> m_results;
    //private TmdbMovie m_selection = null;
      private TmdbMovie m_selection = null;
       private Options options = new Options();
       

    public MovieSearchResultForm( List<MovieResult> movies, string searchterm)
    {
      InitializeComponent();

        txtSearchTerm.Text = string.Format("{0}", searchterm);
      //m_results = _searchResults;
        ShowMovies(movies);
    }
      
      public MovieSearchResultForm(TmdbMovie movie, string searchterm)
      {
          InitializeComponent();
          txtIMDBNum.Text = string.Format("{0}", searchterm);
          ShowMovie(movie);
      }

      private void ShowMovies(List<MovieResult> movies)
      {
          if (!chkAppend.Checked) lvSearchResult.Items.Clear();
          foreach (MovieResult r in movies)
          {
              ListViewItem item = new ListViewItem(r.id.ToString());
              item.SubItems.Add(r.title);
              if (r.release_date != "")
                  item.SubItems.Add(DateTime.Parse(r.release_date).Year.ToString(new CultureInfo("en-US")));
              item.Tag = r;
              lvSearchResult.Items.Add(item);
          }
          if (lvSearchResult.Items.Count > 0)
          {
              lvSearchResult.Items[0].Selected = true;
          }
      }


      private void ShowMovie(TmdbMovie movie)
      {
          if (!chkAppend.Checked) lvSearchResult.Items.Clear();
          ListViewItem item = new ListViewItem(movie.id.ToString());
          item.SubItems.Add(movie.title);
          if (movie.release_date != null)
              item.SubItems.Add(DateTime.Parse(movie.release_date).Year.ToString(new CultureInfo("en-US")));
          item.Tag = movie;
          lvSearchResult.Items.Add(item);

          if (lvSearchResult.Items.Count > 0)
          {
              lvSearchResult.Items[0].Selected = true;
          }
      }
    private void cmdChoose_Click(object sender, EventArgs e)
    {
      if (m_selection == null)
      {
        lblStatus.Text = "Please select a movie first";
      }
      else
      {
        this.DialogResult = DialogResult.OK;
        this.Close();
      }
    }

    private void cmdCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }


    public TmdbMovie Selection
    {
      get { return m_selection; }
      set { m_selection = value; }
    }

    
    private void lvSearchResult_AfterLabelEdit(object sender, LabelEditEventArgs e)
    {

    }

    private void lvSearchResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        TmdbMovie movie = null;
        Tmdb api = new Tmdb("f703f0d3679c29df8855db6c80f382c8");
      if (lvSearchResult.SelectedItems.Count == 1)
      {
         
          switch (lvSearchResult.SelectedItems[0].Tag.GetType().ToString())
          {
              case "WatTmdb.V3.MovieResult":
               
                 movie = api.GetMovieInfo(((MovieResult)lvSearchResult.SelectedItems[0].Tag).id,options.LanguageToReturn);
                  m_selection = movie;
                  break;
              case "WatTmdb.V3.TmdbMovie":
                  movie = (TmdbMovie) lvSearchResult.SelectedItems[0].Tag;
                  m_selection = movie;
                  break;

          }
        
        
        if (movie != null)
        {
            
            TmdbConfiguration config = api.GetConfiguration();
            Uri baseUri = new Uri(config.images.base_url);
            TmdbMovieImages images = api.GetMovieImages(movie.id);
            foreach (Poster poster in images.posters)
            {
                
                        Uri myUri = new Uri(baseUri, "w154" + poster.file_path);
                        pictureBox1.LoadAsync(myUri.AbsoluteUri);
                break;

            }
            txtOverview.Text = movie.overview; //overview
            linkImdb.Text = movie.imdb_id.Equals("")? "": "http://www.imdb.com/title/" + movie.imdb_id;
            if (movie.release_date != null) txtFirstAired.Text = (DateTime.Parse(movie.release_date).Year.ToString(new CultureInfo("en-US")));
         
          //JG  for (int i = 0; i < m_selection.Images.Length; i++)
          /*  {
                if ((m_selection.Images[i].Type == TmdbImageType.poster) && (m_selection.Images[i].Size == "cover"))
                
                {
                    
                    
                    break;
                }
            }
          */
        }

        
         
        
      }
    }

    private void linkImdb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      Process.Start(linkImdb.Text);
    }

    private void button1_Click(object sender, EventArgs e)
    {
        // click this button will "re search" the contents of the movie db based on this term...
        var api = new Tmdb("f703f0d3679c29df8855db6c80f382c8");
        //TmdbMovie[] movies = null;
        TmdbMovieSearch movies = null;
        TmdbMovie movie = null;
        if (txtIMDBNum.Text != "")
        {
            movie = api.GetMovieByIMDB(txtIMDBNum.Text);
            ShowMovie(movie);
        }
        else
        {
            movies = api.SearchMovie(txtSearchTerm.Text, 1, options.LanguageToReturn); //, options.LanguageToReturn);
            ShowMovies(movies.results);
        }
    }

  }
}
