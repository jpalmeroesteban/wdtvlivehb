﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevComponents.AdvTree;
using DevComponents.DotNetBar;

namespace WDTVHubGen.Util
{
    class HelperClasses
    {

    }
    public class NodePasser
    {
        private Node parentNode = null;
        private Node childnode = null;

        public NodePasser (Node parent, Node node )
        {
            parentNode = parent;
            childnode = node;
        }

        public Node ParentNode
        {
            get { return parentNode; }
            set { parentNode = value; }
        }

        public Node ChildNode
        {
            get { return childnode; }
            set { childnode = value; }
        }
    }
}
