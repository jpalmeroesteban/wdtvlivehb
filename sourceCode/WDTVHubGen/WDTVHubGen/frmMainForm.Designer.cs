﻿

using DevComponents.DotNetBar;
using TvdbLib.Cache;


namespace WDTVHubGen
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainForm));
            this.btnPathBrowse = new DevComponents.DotNetBar.ButtonX();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lookupInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manuallySearchForTitleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggressiveLookupInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeExistingXMLThumbToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFolderLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToAnotherFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameBackToOriginalNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnProcess = new DevComponents.DotNetBar.ButtonX();
            this.btnGetFiles = new DevComponents.DotNetBar.ButtonX();
            this.tabControl1 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.objectListViewMovie = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.imageRenderer1 = new BrightIdeasSoftware.ImageRenderer();
            this.olvcVideoType = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcTitle = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcXMLExists = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcThumbPresent = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvSubsPresent = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcDate = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvsSeasons = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcEpisodes = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcDescription = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboPath = new System.Windows.Forms.ComboBox();
            this.btnToggleHidden = new DevComponents.DotNetBar.ButtonX();
            this.btnShowPanelDetail = new DevComponents.DotNetBar.ButtonX();
            this.btnCancelRename = new DevComponents.DotNetBar.ButtonX();
            this.btnRenameFile = new DevComponents.DotNetBar.ButtonX();
            this.txtRenameFile = new System.Windows.Forms.TextBox();
            this.chkShowUnprocessed = new System.Windows.Forms.CheckBox();
            this.btnUserImage = new System.Windows.Forms.Button();
            this.btnSeriesInfo = new DevComponents.DotNetBar.ButtonX();
            this.pbBannerBox = new System.Windows.Forms.PictureBox();
            this.btnSelectAlternateMovies = new DevComponents.DotNetBar.ButtonX();
            this.btnNextImage = new DevComponents.DotNetBar.ButtonX();
            this.lblMovieTitle = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSaveThisOne = new DevComponents.DotNetBar.ButtonX();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tabMovies = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.propertiesGrid = new PropertyGridEx.PropertyGridEx();
            this.tabOptions = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.objectListViewTV = new BrightIdeasSoftware.ObjectListView();
            this.olvcThumbTV = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcSeason = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcEpisode = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcXMLPresentTV = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcThumbPresentTV = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcSubsPresent = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcProcessedDate = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCancelRenameTV = new DevComponents.DotNetBar.ButtonX();
            this.btnRenameTV = new DevComponents.DotNetBar.ButtonX();
            this.txtRenameTV = new System.Windows.Forms.TextBox();
            this.btnShowTVDetailPanel = new DevComponents.DotNetBar.ButtonX();
            this.chkGroupByProcessedTV = new System.Windows.Forms.CheckBox();
            this.txtSeriesPath = new System.Windows.Forms.TextBox();
            this.btnChangePath = new DevComponents.DotNetBar.ButtonX();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetShows = new DevComponents.DotNetBar.ButtonX();
            this.btnProcessAllTV = new DevComponents.DotNetBar.ButtonX();
            this.btnShowMoreSeries = new DevComponents.DotNetBar.ButtonX();
            this.btnTVNextImage = new DevComponents.DotNetBar.ButtonX();
            this.reflectionLabel2 = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.btnSeasonInfo = new DevComponents.DotNetBar.ButtonX();
            this.pbTVBanner = new System.Windows.Forms.PictureBox();
            this.btnTVSaveThisOne = new DevComponents.DotNetBar.ButtonX();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.pbTV = new System.Windows.Forms.PictureBox();
            this.tabTV = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.sbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm100 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm75 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm50 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm25 = new System.Windows.Forms.ToolStripMenuItem();
            this.balloonTip1 = new DevComponents.DotNetBar.BalloonTip();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewMovie)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBannerBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControlPanel3.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewTV)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTVBanner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTV)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPathBrowse
            // 
            this.btnPathBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPathBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.balloonTip1.SetBalloonCaption(this.btnPathBrowse, "Browse");
            this.balloonTip1.SetBalloonText(this.btnPathBrowse, "Click this to browse to a new directory, when done press \"Get Movies\"");
            this.btnPathBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnPathBrowse.Location = new System.Drawing.Point(804, 4);
            this.btnPathBrowse.Name = "btnPathBrowse";
            this.btnPathBrowse.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 5, 0, 5);
            this.btnPathBrowse.Size = new System.Drawing.Size(25, 21);
            this.btnPathBrowse.TabIndex = 1;
            this.btnPathBrowse.Text = "...";
            this.btnPathBrowse.Click += new System.EventHandler(this.btnPathBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Video Directory";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lookupInformationToolStripMenuItem,
            this.manuallySearchForTitleToolStripMenuItem,
            this.deleteFileToolStripMenuItem,
            this.renameFileToolStripMenuItem,
            this.aggressiveLookupInformationToolStripMenuItem,
            this.removeExistingXMLThumbToolStripMenuItem,
            this.openFolderLocationToolStripMenuItem,
            this.moveToAnotherFolderToolStripMenuItem,
            this.renameBackToOriginalNameToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(241, 202);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // lookupInformationToolStripMenuItem
            // 
            this.lookupInformationToolStripMenuItem.Name = "lookupInformationToolStripMenuItem";
            this.lookupInformationToolStripMenuItem.ShortcutKeyDisplayString = "Alt + L";
            this.lookupInformationToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.lookupInformationToolStripMenuItem.Text = "&Lookup Information";
            this.lookupInformationToolStripMenuItem.Click += new System.EventHandler(this.lookupInformationToolStripMenuItem_Click);
            // 
            // manuallySearchForTitleToolStripMenuItem
            // 
            this.manuallySearchForTitleToolStripMenuItem.Name = "manuallySearchForTitleToolStripMenuItem";
            this.manuallySearchForTitleToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.manuallySearchForTitleToolStripMenuItem.Text = "Manually Search for Title";
            this.manuallySearchForTitleToolStripMenuItem.Click += new System.EventHandler(this.manuallySearchForTitleToolStripMenuItem_Click);
            // 
            // deleteFileToolStripMenuItem
            // 
            this.deleteFileToolStripMenuItem.Name = "deleteFileToolStripMenuItem";
            this.deleteFileToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.deleteFileToolStripMenuItem.Text = "&Delete File";
            this.deleteFileToolStripMenuItem.Click += new System.EventHandler(this.deleteFileToolStripMenuItem_Click);
            // 
            // renameFileToolStripMenuItem
            // 
            this.renameFileToolStripMenuItem.Name = "renameFileToolStripMenuItem";
            this.renameFileToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.renameFileToolStripMenuItem.Text = "&Rename File";
            this.renameFileToolStripMenuItem.Click += new System.EventHandler(this.renameFileToolStripMenuItem_Click);
            // 
            // aggressiveLookupInformationToolStripMenuItem
            // 
            this.aggressiveLookupInformationToolStripMenuItem.Name = "aggressiveLookupInformationToolStripMenuItem";
            this.aggressiveLookupInformationToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.aggressiveLookupInformationToolStripMenuItem.Text = "&Aggressive Lookup Information";
            this.aggressiveLookupInformationToolStripMenuItem.Click += new System.EventHandler(this.aggressiveLookupInformationToolStripMenuItem_Click);
            // 
            // removeExistingXMLThumbToolStripMenuItem
            // 
            this.removeExistingXMLThumbToolStripMenuItem.Name = "removeExistingXMLThumbToolStripMenuItem";
            this.removeExistingXMLThumbToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.removeExistingXMLThumbToolStripMenuItem.Text = "Remove Existing XML";
            this.removeExistingXMLThumbToolStripMenuItem.Click += new System.EventHandler(this.removeExistingXMLThumbToolStripMenuItem_Click);
            // 
            // openFolderLocationToolStripMenuItem
            // 
            this.openFolderLocationToolStripMenuItem.Name = "openFolderLocationToolStripMenuItem";
            this.openFolderLocationToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.openFolderLocationToolStripMenuItem.Text = "Open Folder Location";
            this.openFolderLocationToolStripMenuItem.Click += new System.EventHandler(this.openFolderLocationToolStripMenuItem_Click);
            // 
            // moveToAnotherFolderToolStripMenuItem
            // 
            this.moveToAnotherFolderToolStripMenuItem.Name = "moveToAnotherFolderToolStripMenuItem";
            this.moveToAnotherFolderToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.moveToAnotherFolderToolStripMenuItem.Text = "Move to another Folder";
            this.moveToAnotherFolderToolStripMenuItem.Click += new System.EventHandler(this.moveToAnotherFolderToolStripMenuItem_Click);
            // 
            // renameBackToOriginalNameToolStripMenuItem
            // 
            this.renameBackToOriginalNameToolStripMenuItem.Name = "renameBackToOriginalNameToolStripMenuItem";
            this.renameBackToOriginalNameToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.renameBackToOriginalNameToolStripMenuItem.Text = "Rename Back to Original Name";
            this.renameBackToOriginalNameToolStripMenuItem.Click += new System.EventHandler(this.renameBackToOriginalNameToolStripMenuItem_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.balloonTip1.SetBalloonCaption(this.btnProcess, "Process All");
            this.balloonTip1.SetBalloonText(this.btnProcess, "Click Process All to have every \"un processed\" movie looked up and the first foun" +
        "d movie saved as the information.");
            this.btnProcess.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnProcess.Location = new System.Drawing.Point(6, 31);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnProcess.Size = new System.Drawing.Size(85, 23);
            this.btnProcess.TabIndex = 4;
            this.btnProcess.Text = "Process All";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnGetFiles
            // 
            this.btnGetFiles.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGetFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.balloonTip1.SetBalloonCaption(this.btnGetFiles, "Get Movies");
            this.balloonTip1.SetBalloonText(this.btnGetFiles, "This will clear the list view below and refill with the movies that are located i" +
        "n the \"Movie Directory\"");
            this.btnGetFiles.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnGetFiles.Location = new System.Drawing.Point(835, 3);
            this.btnGetFiles.Name = "btnGetFiles";
            this.btnGetFiles.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnGetFiles.Size = new System.Drawing.Size(80, 23);
            this.btnGetFiles.TabIndex = 6;
            this.btnGetFiles.Text = "Get Videos";
            this.btnGetFiles.Click += new System.EventHandler(this.btnGetFiles_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.tabControl1.CanReorderTabs = true;
            this.tabControl1.Controls.Add(this.tabControlPanel1);
            this.tabControl1.Controls.Add(this.tabControlPanel3);
            this.tabControl1.Controls.Add(this.tabControlPanel2);
            this.tabControl1.Controls.Add(this.tabControlPanel4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.SelectedTabIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1067, 663);
            this.tabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabControl1.TabIndex = 8;
            this.tabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl1.Tabs.Add(this.tabMovies);
            this.tabControl1.Tabs.Add(this.tabTV);
            this.tabControl1.Tabs.Add(this.tabItem1);
            this.tabControl1.Tabs.Add(this.tabOptions);
            this.tabControl1.Text = "tabControl1";
            this.tabControl1.SelectedTabChanging += new DevComponents.DotNetBar.TabStrip.SelectedTabChangingEventHandler(this.tabControl1_SelectedTabChanging);
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.splitContainer1);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 25);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1067, 638);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tabMovies;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(1, 1);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.objectListViewMovie);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnUserImage);
            this.splitContainer1.Panel2.Controls.Add(this.btnSeriesInfo);
            this.splitContainer1.Panel2.Controls.Add(this.pbBannerBox);
            this.splitContainer1.Panel2.Controls.Add(this.btnSelectAlternateMovies);
            this.splitContainer1.Panel2.Controls.Add(this.btnNextImage);
            this.splitContainer1.Panel2.Controls.Add(this.lblMovieTitle);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel2.Controls.Add(this.btnSaveThisOne);
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer1.Panel2MinSize = 295;
            this.splitContainer1.Size = new System.Drawing.Size(1065, 636);
            this.splitContainer1.SplitterDistance = 297;
            this.splitContainer1.TabIndex = 9;
            // 
            // objectListViewMovie
            // 
            this.objectListViewMovie.AllColumns.Add(this.olvColumn1);
            this.objectListViewMovie.AllColumns.Add(this.olvColumn7);
            this.objectListViewMovie.AllColumns.Add(this.olvcVideoType);
            this.objectListViewMovie.AllColumns.Add(this.olvcTitle);
            this.objectListViewMovie.AllColumns.Add(this.olvColumn2);
            this.objectListViewMovie.AllColumns.Add(this.olvcXMLExists);
            this.objectListViewMovie.AllColumns.Add(this.olvcThumbPresent);
            this.objectListViewMovie.AllColumns.Add(this.olvSubsPresent);
            this.objectListViewMovie.AllColumns.Add(this.olvcDate);
            this.objectListViewMovie.AllColumns.Add(this.olvColumn5);
            this.objectListViewMovie.AllColumns.Add(this.olvsSeasons);
            this.objectListViewMovie.AllColumns.Add(this.olvcEpisodes);
            this.objectListViewMovie.AllColumns.Add(this.olvcDescription);
            this.objectListViewMovie.AlternateRowBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.objectListViewMovie.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn7,
            this.olvcVideoType,
            this.olvcTitle,
            this.olvColumn2,
            this.olvcXMLExists,
            this.olvcThumbPresent,
            this.olvSubsPresent,
            this.olvcDate,
            this.olvColumn5,
            this.olvsSeasons,
            this.olvcEpisodes,
            this.olvcDescription});
            this.objectListViewMovie.ContextMenuStrip = this.contextMenuStrip1;
            this.objectListViewMovie.Cursor = System.Windows.Forms.Cursors.Default;
            this.objectListViewMovie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListViewMovie.EmptyListMsg = "Select a Directory and press the Button \"Get Movies\"";
            this.objectListViewMovie.FullRowSelect = true;
            this.objectListViewMovie.HideSelection = false;
            this.objectListViewMovie.LargeImageList = this.imageList1;
            this.objectListViewMovie.Location = new System.Drawing.Point(0, 59);
            this.objectListViewMovie.Name = "objectListViewMovie";
            this.objectListViewMovie.OverlayText.Alignment = System.Drawing.ContentAlignment.BottomCenter;
            this.objectListViewMovie.OverlayText.BackColor = System.Drawing.Color.Red;
            this.objectListViewMovie.OverlayText.BorderColor = System.Drawing.Color.Black;
            this.objectListViewMovie.OverlayText.BorderWidth = 2F;
            this.objectListViewMovie.OverlayText.Text = "";
            this.objectListViewMovie.RowHeight = 100;
            this.objectListViewMovie.Size = new System.Drawing.Size(1065, 238);
            this.objectListViewMovie.SmallImageList = this.imageList1;
            this.objectListViewMovie.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.objectListViewMovie.TabIndex = 0;
            this.objectListViewMovie.UseAlternatingBackColors = true;
            this.objectListViewMovie.UseCompatibleStateImageBehavior = false;
            this.objectListViewMovie.UseHotItem = true;
            this.objectListViewMovie.UseTranslucentHotItem = true;
            this.objectListViewMovie.View = System.Windows.Forms.View.Details;
            this.objectListViewMovie.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.objectListViewMovie_FormatRow);
            this.objectListViewMovie.SelectionChanged += new System.EventHandler(this.objectListViewMovie_SelectionChanged);
            this.objectListViewMovie.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.objectListViewMovie_ColumnWidthChanged);
            this.objectListViewMovie.SelectedIndexChanged += new System.EventHandler(this.objectListViewMovie_SelectedIndexChanged);
            this.objectListViewMovie.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.objectListView1_MouseDoubleClick);
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Title";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.IsVisible = false;
            this.olvColumn1.Width = 2;
            // 
            // olvColumn7
            // 
            this.olvColumn7.AspectName = "ThumbPath";
            this.olvColumn7.CellPadding = null;
            this.olvColumn7.Renderer = this.imageRenderer1;
            this.olvColumn7.Text = "";
            this.olvColumn7.Width = 74;
            // 
            // olvcVideoType
            // 
            this.olvcVideoType.AspectName = "VideoType";
            this.olvcVideoType.CellPadding = null;
            this.olvcVideoType.Text = "Type";
            // 
            // olvcTitle
            // 
            this.olvcTitle.AspectName = "Title";
            this.olvcTitle.CellPadding = null;
            this.olvcTitle.Text = "Title";
            this.olvcTitle.Width = 200;
            this.olvcTitle.WordWrap = true;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "CleanFileName";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Clean File Name";
            this.olvColumn2.Width = 100;
            // 
            // olvcXMLExists
            // 
            this.olvcXMLExists.AspectName = "XmlPresent";
            this.olvcXMLExists.CellPadding = null;
            this.olvcXMLExists.CheckBoxes = true;
            this.olvcXMLExists.IsEditable = false;
            this.olvcXMLExists.Text = "XML?";
            this.olvcXMLExists.Width = 40;
            // 
            // olvcThumbPresent
            // 
            this.olvcThumbPresent.AspectName = "ThumbnailPresent";
            this.olvcThumbPresent.CellPadding = null;
            this.olvcThumbPresent.CheckBoxes = true;
            this.olvcThumbPresent.IsEditable = false;
            this.olvcThumbPresent.Text = "Thumb?";
            this.olvcThumbPresent.Width = 55;
            // 
            // olvSubsPresent
            // 
            this.olvSubsPresent.AspectName = "SubsPresent";
            this.olvSubsPresent.CellPadding = null;
            this.olvSubsPresent.CheckBoxes = true;
            this.olvSubsPresent.Text = "Subs Present";
            // 
            // olvcDate
            // 
            this.olvcDate.AspectName = "ProcessedDate";
            this.olvcDate.AspectToStringFormat = "{0:d}";
            this.olvcDate.CellPadding = null;
            this.olvcDate.Text = "Date";
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "FullFileName";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.Text = "Full Path";
            this.olvColumn5.Width = 150;
            this.olvColumn5.WordWrap = true;
            // 
            // olvsSeasons
            // 
            this.olvsSeasons.AspectName = "Season";
            this.olvsSeasons.CellPadding = null;
            this.olvsSeasons.IsTileViewColumn = true;
            this.olvsSeasons.Text = "Season";
            // 
            // olvcEpisodes
            // 
            this.olvcEpisodes.AspectName = "Episode";
            this.olvcEpisodes.CellPadding = null;
            this.olvcEpisodes.IsTileViewColumn = true;
            this.olvcEpisodes.Text = "Ep";
            this.olvcEpisodes.Width = 40;
            // 
            // olvcDescription
            // 
            this.olvcDescription.AspectName = "Description";
            this.olvcDescription.CellPadding = null;
            this.olvcDescription.IsTileViewColumn = true;
            this.olvcDescription.Text = "Description";
            this.olvcDescription.Width = 160;
            this.olvcDescription.WordWrap = true;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboPath);
            this.panel1.Controls.Add(this.btnToggleHidden);
            this.panel1.Controls.Add(this.btnShowPanelDetail);
            this.panel1.Controls.Add(this.btnProcess);
            this.panel1.Controls.Add(this.btnCancelRename);
            this.panel1.Controls.Add(this.btnRenameFile);
            this.panel1.Controls.Add(this.txtRenameFile);
            this.panel1.Controls.Add(this.chkShowUnprocessed);
            this.panel1.Controls.Add(this.btnPathBrowse);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnGetFiles);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1065, 59);
            this.panel1.TabIndex = 9;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // cboPath
            // 
            this.cboPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.balloonTip1.SetBalloonCaption(this.cboPath, "Video Directory");
            this.balloonTip1.SetBalloonText(this.cboPath, "The Directory that videos exist in, this can be a relative or absolute path, as w" +
        "ell as a network UNC");
            this.cboPath.FormattingEnabled = true;
            this.cboPath.Location = new System.Drawing.Point(83, 4);
            this.cboPath.Name = "cboPath";
            this.cboPath.Size = new System.Drawing.Size(721, 21);
            this.cboPath.TabIndex = 16;
            this.cboPath.SelectedIndexChanged += new System.EventHandler(this.cboPath_SelectedIndexChanged);
            this.cboPath.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboPath_KeyDown);
            // 
            // btnToggleHidden
            // 
            this.btnToggleHidden.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnToggleHidden.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.balloonTip1.SetBalloonCaption(this.btnToggleHidden, "Toggle Hidden JPGs");
            this.balloonTip1.SetBalloonText(this.btnToggleHidden, "Toggles (on or off) whether a JPG is hidden (prepended with a .)");
            this.btnToggleHidden.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnToggleHidden.Location = new System.Drawing.Point(921, 3);
            this.btnToggleHidden.Name = "btnToggleHidden";
            this.btnToggleHidden.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnToggleHidden.Size = new System.Drawing.Size(139, 23);
            this.btnToggleHidden.TabIndex = 15;
            this.btnToggleHidden.Text = "Toggle Hidden JPGs";
            this.btnToggleHidden.Click += new System.EventHandler(this.btnToggleHidden_Click);
            // 
            // btnShowPanelDetail
            // 
            this.btnShowPanelDetail.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.btnShowPanelDetail.AutoCheckOnClick = true;
            this.balloonTip1.SetBalloonCaption(this.btnShowPanelDetail, "Show Detail Panel");
            this.balloonTip1.SetBalloonText(this.btnShowPanelDetail, "Shows/Hides the detail panel at the bottom of the screen");
            this.btnShowPanelDetail.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnShowPanelDetail.Location = new System.Drawing.Point(225, 33);
            this.btnShowPanelDetail.Name = "btnShowPanelDetail";
            this.btnShowPanelDetail.Size = new System.Drawing.Size(108, 21);
            this.btnShowPanelDetail.TabIndex = 14;
            this.btnShowPanelDetail.Text = "Show Detail Panel";
            this.btnShowPanelDetail.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btnCancelRename
            // 
            this.btnCancelRename.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelRename.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancelRename.Location = new System.Drawing.Point(964, 31);
            this.btnCancelRename.Name = "btnCancelRename";
            this.btnCancelRename.Size = new System.Drawing.Size(96, 23);
            this.btnCancelRename.TabIndex = 13;
            this.btnCancelRename.Text = "Cancel Rename";
            this.btnCancelRename.Visible = false;
            this.btnCancelRename.Click += new System.EventHandler(this.btnCancelRename_Click);
            // 
            // btnRenameFile
            // 
            this.btnRenameFile.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRenameFile.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnRenameFile.Location = new System.Drawing.Point(885, 31);
            this.btnRenameFile.Name = "btnRenameFile";
            this.btnRenameFile.Size = new System.Drawing.Size(75, 23);
            this.btnRenameFile.TabIndex = 12;
            this.btnRenameFile.Text = "Rename File";
            this.btnRenameFile.Visible = false;
            this.btnRenameFile.Click += new System.EventHandler(this.btnRenameFile_Click);
            // 
            // txtRenameFile
            // 
            this.txtRenameFile.Location = new System.Drawing.Point(342, 33);
            this.txtRenameFile.Name = "txtRenameFile";
            this.txtRenameFile.Size = new System.Drawing.Size(536, 20);
            this.txtRenameFile.TabIndex = 11;
            this.txtRenameFile.Visible = false;
            this.txtRenameFile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRenameFile_KeyDown);
            // 
            // chkShowUnprocessed
            // 
            this.chkShowUnprocessed.AutoSize = true;
            this.chkShowUnprocessed.BackColor = System.Drawing.Color.Transparent;
            this.balloonTip1.SetBalloonCaption(this.chkShowUnprocessed, "Group By");
            this.balloonTip1.SetBalloonText(this.chkShowUnprocessed, "Allows the Grouping to show processed and unprocessed. Click on the XML? header a" +
        "lso to change sort order");
            this.chkShowUnprocessed.Checked = true;
            this.chkShowUnprocessed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowUnprocessed.Location = new System.Drawing.Point(99, 35);
            this.chkShowUnprocessed.Name = "chkShowUnprocessed";
            this.chkShowUnprocessed.Size = new System.Drawing.Size(123, 17);
            this.chkShowUnprocessed.TabIndex = 9;
            this.chkShowUnprocessed.Text = "Group By Processed";
            this.chkShowUnprocessed.UseVisualStyleBackColor = false;
            this.chkShowUnprocessed.CheckedChanged += new System.EventHandler(this.chkShowUnprocessed_CheckedChanged);
            // 
            // btnUserImage
            // 
            this.btnUserImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserImage.Location = new System.Drawing.Point(141, 196);
            this.btnUserImage.Name = "btnUserImage";
            this.btnUserImage.Size = new System.Drawing.Size(55, 23);
            this.btnUserImage.TabIndex = 22;
            this.btnUserImage.Text = "User Image";
            this.btnUserImage.UseVisualStyleBackColor = true;
            this.btnUserImage.Click += new System.EventHandler(this.btnUserImage_Click);
            // 
            // btnSeriesInfo
            // 
            this.btnSeriesInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.balloonTip1.SetBalloonCaption(this.btnSeriesInfo, "Next Image");
            this.balloonTip1.SetBalloonText(this.btnSeriesInfo, "Press to cycle through alternate images for a new movie");
            this.btnSeriesInfo.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSeriesInfo.Enabled = false;
            this.btnSeriesInfo.Location = new System.Drawing.Point(146, 73);
            this.btnSeriesInfo.Name = "btnSeriesInfo";
            this.btnSeriesInfo.Size = new System.Drawing.Size(43, 71);
            this.btnSeriesInfo.TabIndex = 21;
            this.btnSeriesInfo.Text = "Series Folder Icon Save";
            this.btnSeriesInfo.Click += new System.EventHandler(this.btnSeasonInfo_Click);
            // 
            // pbBannerBox
            // 
            this.pbBannerBox.Location = new System.Drawing.Point(11, 225);
            this.pbBannerBox.Name = "pbBannerBox";
            this.pbBannerBox.Size = new System.Drawing.Size(185, 101);
            this.pbBannerBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBannerBox.TabIndex = 20;
            this.pbBannerBox.TabStop = false;
            // 
            // btnSelectAlternateMovies
            // 
            this.btnSelectAlternateMovies.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSelectAlternateMovies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectAlternateMovies.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSelectAlternateMovies.Enabled = false;
            this.btnSelectAlternateMovies.Location = new System.Drawing.Point(755, 9);
            this.btnSelectAlternateMovies.Name = "btnSelectAlternateMovies";
            this.btnSelectAlternateMovies.Size = new System.Drawing.Size(195, 23);
            this.btnSelectAlternateMovies.TabIndex = 19;
            this.btnSelectAlternateMovies.Text = "# Found";
            this.btnSelectAlternateMovies.Click += new System.EventHandler(this.lblMultipleFilms_MouseDoubleClick);
            // 
            // btnNextImage
            // 
            this.btnNextImage.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.balloonTip1.SetBalloonCaption(this.btnNextImage, "Next Image");
            this.balloonTip1.SetBalloonText(this.btnNextImage, "Press to cycle through alternate images for a new movie");
            this.btnNextImage.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnNextImage.Enabled = false;
            this.btnNextImage.Location = new System.Drawing.Point(137, 41);
            this.btnNextImage.Name = "btnNextImage";
            this.btnNextImage.Size = new System.Drawing.Size(23, 23);
            this.btnNextImage.TabIndex = 13;
            this.btnNextImage.Text = ">";
            this.btnNextImage.Click += new System.EventHandler(this.btnNextImage_Click);
            // 
            // lblMovieTitle
            // 
            this.lblMovieTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblMovieTitle.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.lblMovieTitle.BackgroundStyle.BackColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblMovieTitle.BackgroundStyle.BackColorGradientAngle = 90;
            this.lblMovieTitle.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.lblMovieTitle.BackgroundStyle.BorderBottomWidth = 3;
            this.lblMovieTitle.BackgroundStyle.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblMovieTitle.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.lblMovieTitle.BackgroundStyle.BorderLeftWidth = 3;
            this.lblMovieTitle.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.lblMovieTitle.BackgroundStyle.BorderRightWidth = 3;
            this.lblMovieTitle.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.lblMovieTitle.BackgroundStyle.BorderTopWidth = 3;
            this.lblMovieTitle.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.lblMovieTitle.BackgroundStyle.PaddingBottom = 3;
            this.lblMovieTitle.BackgroundStyle.PaddingLeft = 10;
            this.lblMovieTitle.Location = new System.Drawing.Point(11, 5);
            this.lblMovieTitle.Name = "lblMovieTitle";
            this.lblMovieTitle.ReflectionEnabled = false;
            this.lblMovieTitle.Size = new System.Drawing.Size(719, 32);
            this.lblMovieTitle.TabIndex = 14;
            this.lblMovieTitle.Text = "Movie Title Here";
            this.lblMovieTitle.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(11, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(124, 179);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnSaveThisOne
            // 
            this.btnSaveThisOne.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSaveThisOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.balloonTip1.SetBalloonCaption(this.btnSaveThisOne, "Save This One");
            this.balloonTip1.SetBalloonText(this.btnSaveThisOne, "Saves the current movie with the custom image selected. Immediate.");
            this.btnSaveThisOne.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSaveThisOne.Enabled = false;
            this.btnSaveThisOne.Location = new System.Drawing.Point(960, 9);
            this.btnSaveThisOne.Name = "btnSaveThisOne";
            this.btnSaveThisOne.Size = new System.Drawing.Size(94, 25);
            this.btnSaveThisOne.TabIndex = 12;
            this.btnSaveThisOne.Text = "Save This One";
            this.btnSaveThisOne.Click += new System.EventHandler(this.btnProcessAlternate_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(202, 43);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(852, 283);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // tabMovies
            // 
            this.tabMovies.AttachedControl = this.tabControlPanel1;
            this.tabMovies.Icon = ((System.Drawing.Icon)(resources.GetObject("tabMovies.Icon")));
            this.tabMovies.Name = "tabMovies";
            this.tabMovies.Text = "Videos";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.propertiesGrid);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 25);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1067, 638);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.tabOptions;
            // 
            // propertiesGrid
            // 
            // 
            // 
            // 
            this.propertiesGrid.DocCommentDescription.AutoEllipsis = true;
            this.propertiesGrid.DocCommentDescription.AutoSize = true;
            this.propertiesGrid.DocCommentDescription.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertiesGrid.DocCommentDescription.Location = new System.Drawing.Point(3, 18);
            this.propertiesGrid.DocCommentDescription.MinimumSize = new System.Drawing.Size(0, 500);
            this.propertiesGrid.DocCommentDescription.Name = "";
            this.propertiesGrid.DocCommentDescription.Size = new System.Drawing.Size(0, 500);
            this.propertiesGrid.DocCommentDescription.TabIndex = 1;
            this.propertiesGrid.DocCommentImage = null;
            // 
            // 
            // 
            this.propertiesGrid.DocCommentTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertiesGrid.DocCommentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.propertiesGrid.DocCommentTitle.Location = new System.Drawing.Point(3, 3);
            this.propertiesGrid.DocCommentTitle.Name = "";
            this.propertiesGrid.DocCommentTitle.Size = new System.Drawing.Size(1059, 15);
            this.propertiesGrid.DocCommentTitle.TabIndex = 0;
            this.propertiesGrid.DocCommentTitle.Text = "Examples and Descriptions";
            this.propertiesGrid.DocCommentTitle.UseMnemonic = false;
            this.propertiesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertiesGrid.Location = new System.Drawing.Point(1, 1);
            this.propertiesGrid.Name = "propertiesGrid";
            this.propertiesGrid.Size = new System.Drawing.Size(1065, 636);
            this.propertiesGrid.TabIndex = 0;
            // 
            // 
            // 
            this.propertiesGrid.ToolStrip.AccessibleName = "ToolBar";
            this.propertiesGrid.ToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.propertiesGrid.ToolStrip.AllowMerge = false;
            this.propertiesGrid.ToolStrip.AutoSize = false;
            this.propertiesGrid.ToolStrip.CanOverflow = false;
            this.propertiesGrid.ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.propertiesGrid.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.propertiesGrid.ToolStrip.Location = new System.Drawing.Point(0, 1);
            this.propertiesGrid.ToolStrip.Name = "";
            this.propertiesGrid.ToolStrip.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.propertiesGrid.ToolStrip.Size = new System.Drawing.Size(1065, 25);
            this.propertiesGrid.ToolStrip.TabIndex = 1;
            this.propertiesGrid.ToolStrip.TabStop = true;
            this.propertiesGrid.ToolStrip.Text = "PropertyGridToolBar";
            this.propertiesGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertiesGrid_PropertyValueChanged);
            // 
            // tabOptions
            // 
            this.tabOptions.AttachedControl = this.tabControlPanel3;
            this.tabOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("tabOptions.Icon")));
            this.tabOptions.Name = "tabOptions";
            this.tabOptions.Text = "Options";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.splitContainer2);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 25);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1067, 638);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tabTV;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(1, 1);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.objectListViewTV);
            this.splitContainer2.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.btnShowMoreSeries);
            this.splitContainer2.Panel2.Controls.Add(this.btnTVNextImage);
            this.splitContainer2.Panel2.Controls.Add(this.reflectionLabel2);
            this.splitContainer2.Panel2.Controls.Add(this.btnSeasonInfo);
            this.splitContainer2.Panel2.Controls.Add(this.pbTVBanner);
            this.splitContainer2.Panel2.Controls.Add(this.btnTVSaveThisOne);
            this.splitContainer2.Panel2.Controls.Add(this.flowLayoutPanel2);
            this.splitContainer2.Panel2.Controls.Add(this.pbTV);
            this.splitContainer2.Size = new System.Drawing.Size(1065, 636);
            this.splitContainer2.SplitterDistance = 331;
            this.splitContainer2.TabIndex = 10;
            // 
            // objectListViewTV
            // 
            this.objectListViewTV.AllColumns.Add(this.olvcThumbTV);
            this.objectListViewTV.AllColumns.Add(this.olvColumn3);
            this.objectListViewTV.AllColumns.Add(this.olvColumn4);
            this.objectListViewTV.AllColumns.Add(this.olvcSeason);
            this.objectListViewTV.AllColumns.Add(this.olvcEpisode);
            this.objectListViewTV.AllColumns.Add(this.olvcXMLPresentTV);
            this.objectListViewTV.AllColumns.Add(this.olvcThumbPresentTV);
            this.objectListViewTV.AllColumns.Add(this.olvcSubsPresent);
            this.objectListViewTV.AllColumns.Add(this.olvcProcessedDate);
            this.objectListViewTV.AllColumns.Add(this.olvColumn10);
            this.objectListViewTV.AllColumns.Add(this.olvColumn11);
            this.objectListViewTV.AlternateRowBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.objectListViewTV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvcThumbTV,
            this.olvColumn3,
            this.olvColumn4,
            this.olvcSeason,
            this.olvcEpisode,
            this.olvcXMLPresentTV,
            this.olvcThumbPresentTV,
            this.olvcSubsPresent,
            this.olvcProcessedDate,
            this.olvColumn10,
            this.olvColumn11});
            this.objectListViewTV.ContextMenuStrip = this.contextMenuStrip1;
            this.objectListViewTV.Cursor = System.Windows.Forms.Cursors.Default;
            this.objectListViewTV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListViewTV.EmptyListMsg = "Select a Directory and press the Button \"Get Shows\"";
            this.objectListViewTV.FullRowSelect = true;
            this.objectListViewTV.HideSelection = false;
            this.objectListViewTV.Location = new System.Drawing.Point(0, 59);
            this.objectListViewTV.Name = "objectListViewTV";
            this.objectListViewTV.OverlayText.Alignment = System.Drawing.ContentAlignment.BottomCenter;
            this.objectListViewTV.OverlayText.BackColor = System.Drawing.Color.Red;
            this.objectListViewTV.OverlayText.BorderColor = System.Drawing.Color.Black;
            this.objectListViewTV.OverlayText.BorderWidth = 2F;
            this.objectListViewTV.OverlayText.Text = "";
            this.objectListViewTV.RowHeight = 100;
            this.objectListViewTV.Size = new System.Drawing.Size(1065, 272);
            this.objectListViewTV.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.objectListViewTV.TabIndex = 10;
            this.objectListViewTV.UseAlternatingBackColors = true;
            this.objectListViewTV.UseCompatibleStateImageBehavior = false;
            this.objectListViewTV.UseHotItem = true;
            this.objectListViewTV.UseTranslucentHotItem = true;
            this.objectListViewTV.View = System.Windows.Forms.View.Details;
            this.objectListViewTV.SelectionChanged += new System.EventHandler(this.objectListViewTV_SelectionChanged);
            this.objectListViewTV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.objectListViewTV_MouseDoubleClick);
            // 
            // olvcThumbTV
            // 
            this.olvcThumbTV.AspectName = "ThumbPath";
            this.olvcThumbTV.CellPadding = null;
            this.olvcThumbTV.Renderer = this.imageRenderer1;
            this.olvcThumbTV.Text = "";
            this.olvcThumbTV.Width = 74;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Title";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Title";
            this.olvColumn3.Width = 200;
            this.olvColumn3.WordWrap = true;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "CleanFileName";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Clean File Name";
            this.olvColumn4.Width = 100;
            // 
            // olvcSeason
            // 
            this.olvcSeason.AspectName = "Season";
            this.olvcSeason.CellPadding = null;
            this.olvcSeason.IsTileViewColumn = true;
            this.olvcSeason.Text = "Season";
            // 
            // olvcEpisode
            // 
            this.olvcEpisode.AspectName = "Episode";
            this.olvcEpisode.CellPadding = null;
            this.olvcEpisode.IsTileViewColumn = true;
            this.olvcEpisode.Text = "Episode";
            // 
            // olvcXMLPresentTV
            // 
            this.olvcXMLPresentTV.AspectName = "XmlPresent";
            this.olvcXMLPresentTV.CellPadding = null;
            this.olvcXMLPresentTV.CheckBoxes = true;
            this.olvcXMLPresentTV.IsEditable = false;
            this.olvcXMLPresentTV.Text = "XML?";
            this.olvcXMLPresentTV.Width = 40;
            // 
            // olvcThumbPresentTV
            // 
            this.olvcThumbPresentTV.AspectName = "ThumbnailPresent";
            this.olvcThumbPresentTV.CellPadding = null;
            this.olvcThumbPresentTV.CheckBoxes = true;
            this.olvcThumbPresentTV.IsEditable = false;
            this.olvcThumbPresentTV.Text = "Thumb?";
            this.olvcThumbPresentTV.Width = 55;
            // 
            // olvcSubsPresent
            // 
            this.olvcSubsPresent.AspectName = "SubsPresent";
            this.olvcSubsPresent.CellPadding = null;
            this.olvcSubsPresent.CheckBoxes = true;
            this.olvcSubsPresent.Text = "Subs?";
            // 
            // olvcProcessedDate
            // 
            this.olvcProcessedDate.AspectName = "ProcessedDate";
            this.olvcProcessedDate.AspectToStringFormat = "{0:d}";
            this.olvcProcessedDate.CellPadding = null;
            this.olvcProcessedDate.Text = "Date";
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "FullFileName";
            this.olvColumn10.CellPadding = null;
            this.olvColumn10.Text = "Full Path";
            this.olvColumn10.Width = 150;
            this.olvColumn10.WordWrap = true;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Description";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.FillsFreeSpace = true;
            this.olvColumn11.IsTileViewColumn = true;
            this.olvColumn11.Text = "Description";
            this.olvColumn11.WordWrap = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCancelRenameTV);
            this.panel2.Controls.Add(this.btnRenameTV);
            this.panel2.Controls.Add(this.txtRenameTV);
            this.panel2.Controls.Add(this.btnShowTVDetailPanel);
            this.panel2.Controls.Add(this.chkGroupByProcessedTV);
            this.panel2.Controls.Add(this.txtSeriesPath);
            this.panel2.Controls.Add(this.btnChangePath);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnGetShows);
            this.panel2.Controls.Add(this.btnProcessAllTV);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1065, 59);
            this.panel2.TabIndex = 9;
            // 
            // btnCancelRenameTV
            // 
            this.btnCancelRenameTV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelRenameTV.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnCancelRenameTV.Location = new System.Drawing.Point(808, 31);
            this.btnCancelRenameTV.Name = "btnCancelRenameTV";
            this.btnCancelRenameTV.Size = new System.Drawing.Size(96, 23);
            this.btnCancelRenameTV.TabIndex = 18;
            this.btnCancelRenameTV.Text = "Cancel Rename";
            this.btnCancelRenameTV.Visible = false;
            this.btnCancelRenameTV.Click += new System.EventHandler(this.btnCancelRenameTV_Click);
            // 
            // btnRenameTV
            // 
            this.btnRenameTV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRenameTV.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnRenameTV.Location = new System.Drawing.Point(729, 31);
            this.btnRenameTV.Name = "btnRenameTV";
            this.btnRenameTV.Size = new System.Drawing.Size(75, 23);
            this.btnRenameTV.TabIndex = 17;
            this.btnRenameTV.Text = "Rename File";
            this.btnRenameTV.Visible = false;
            this.btnRenameTV.Click += new System.EventHandler(this.btnRenameTV_Click);
            // 
            // txtRenameTV
            // 
            this.txtRenameTV.Location = new System.Drawing.Point(342, 33);
            this.txtRenameTV.Name = "txtRenameTV";
            this.txtRenameTV.Size = new System.Drawing.Size(380, 20);
            this.txtRenameTV.TabIndex = 16;
            this.txtRenameTV.Visible = false;
            // 
            // btnShowTVDetailPanel
            // 
            this.btnShowTVDetailPanel.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.btnShowTVDetailPanel.AutoCheckOnClick = true;
            this.btnShowTVDetailPanel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnShowTVDetailPanel.Location = new System.Drawing.Point(228, 33);
            this.btnShowTVDetailPanel.Name = "btnShowTVDetailPanel";
            this.btnShowTVDetailPanel.Size = new System.Drawing.Size(108, 21);
            this.btnShowTVDetailPanel.TabIndex = 15;
            this.btnShowTVDetailPanel.Text = "Show Detail Panel";
            this.btnShowTVDetailPanel.Click += new System.EventHandler(this.btnShowTVDetailPanel_Click);
            // 
            // chkGroupByProcessedTV
            // 
            this.chkGroupByProcessedTV.AutoSize = true;
            this.chkGroupByProcessedTV.Checked = true;
            this.chkGroupByProcessedTV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGroupByProcessedTV.Location = new System.Drawing.Point(100, 36);
            this.chkGroupByProcessedTV.Name = "chkGroupByProcessedTV";
            this.chkGroupByProcessedTV.Size = new System.Drawing.Size(122, 17);
            this.chkGroupByProcessedTV.TabIndex = 9;
            this.chkGroupByProcessedTV.Text = "Group by Processed";
            this.chkGroupByProcessedTV.UseVisualStyleBackColor = true;
            this.chkGroupByProcessedTV.CheckedChanged += new System.EventHandler(this.chkGroupByProcessedTV_CheckedChanged);
            // 
            // txtSeriesPath
            // 
            this.txtSeriesPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSeriesPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeriesPath.Location = new System.Drawing.Point(83, 4);
            this.txtSeriesPath.Name = "txtSeriesPath";
            this.txtSeriesPath.Size = new System.Drawing.Size(867, 21);
            this.txtSeriesPath.TabIndex = 0;
            this.txtSeriesPath.Text = "c:\\";
            // 
            // btnChangePath
            // 
            this.btnChangePath.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChangePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePath.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnChangePath.Location = new System.Drawing.Point(956, 3);
            this.btnChangePath.Name = "btnChangePath";
            this.btnChangePath.Size = new System.Drawing.Size(25, 23);
            this.btnChangePath.TabIndex = 1;
            this.btnChangePath.Text = "...";
            this.btnChangePath.Click += new System.EventHandler(this.btnChangePath_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Series Directory";
            // 
            // btnGetShows
            // 
            this.btnGetShows.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGetShows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetShows.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnGetShows.Location = new System.Drawing.Point(987, 3);
            this.btnGetShows.Name = "btnGetShows";
            this.btnGetShows.Size = new System.Drawing.Size(75, 23);
            this.btnGetShows.TabIndex = 6;
            this.btnGetShows.Text = "Get Shows";
            // 
            // btnProcessAllTV
            // 
            this.btnProcessAllTV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnProcessAllTV.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnProcessAllTV.Location = new System.Drawing.Point(10, 32);
            this.btnProcessAllTV.Name = "btnProcessAllTV";
            this.btnProcessAllTV.Size = new System.Drawing.Size(84, 22);
            this.btnProcessAllTV.TabIndex = 7;
            this.btnProcessAllTV.Text = "Process All";
            this.btnProcessAllTV.Click += new System.EventHandler(this.btnTVSelectAll_Click);
            // 
            // btnShowMoreSeries
            // 
            this.btnShowMoreSeries.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowMoreSeries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowMoreSeries.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnShowMoreSeries.Enabled = false;
            this.btnShowMoreSeries.Location = new System.Drawing.Point(811, 7);
            this.btnShowMoreSeries.Name = "btnShowMoreSeries";
            this.btnShowMoreSeries.Size = new System.Drawing.Size(145, 23);
            this.btnShowMoreSeries.TabIndex = 18;
            this.btnShowMoreSeries.Text = "# Found";
            this.btnShowMoreSeries.Click += new System.EventHandler(this.lblMultpleTV_Click);
            // 
            // btnTVNextImage
            // 
            this.btnTVNextImage.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTVNextImage.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnTVNextImage.Enabled = false;
            this.btnTVNextImage.Location = new System.Drawing.Point(136, 9);
            this.btnTVNextImage.Name = "btnTVNextImage";
            this.btnTVNextImage.Size = new System.Drawing.Size(23, 23);
            this.btnTVNextImage.TabIndex = 17;
            this.btnTVNextImage.Text = ">";
            // 
            // reflectionLabel2
            // 
            this.reflectionLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.reflectionLabel2.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.reflectionLabel2.BackgroundStyle.BackColor2 = System.Drawing.Color.WhiteSmoke;
            this.reflectionLabel2.BackgroundStyle.BackColorGradientAngle = 90;
            this.reflectionLabel2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.reflectionLabel2.BackgroundStyle.BorderBottomWidth = 3;
            this.reflectionLabel2.BackgroundStyle.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.reflectionLabel2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.reflectionLabel2.BackgroundStyle.BorderLeftWidth = 3;
            this.reflectionLabel2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.reflectionLabel2.BackgroundStyle.BorderRightWidth = 3;
            this.reflectionLabel2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.reflectionLabel2.BackgroundStyle.BorderTopWidth = 3;
            this.reflectionLabel2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.reflectionLabel2.BackgroundStyle.PaddingBottom = 3;
            this.reflectionLabel2.BackgroundStyle.PaddingLeft = 10;
            this.reflectionLabel2.Location = new System.Drawing.Point(173, 2);
            this.reflectionLabel2.Name = "reflectionLabel2";
            this.reflectionLabel2.ReflectionEnabled = false;
            this.reflectionLabel2.Size = new System.Drawing.Size(518, 32);
            this.reflectionLabel2.TabIndex = 16;
            this.reflectionLabel2.Text = "Show Title Here";
            this.reflectionLabel2.Visible = false;
            // 
            // btnSeasonInfo
            // 
            this.btnSeasonInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSeasonInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSeasonInfo.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnSeasonInfo.Enabled = false;
            this.btnSeasonInfo.Location = new System.Drawing.Point(697, 7);
            this.btnSeasonInfo.Name = "btnSeasonInfo";
            this.btnSeasonInfo.Size = new System.Drawing.Size(108, 23);
            this.btnSeasonInfo.TabIndex = 15;
            this.btnSeasonInfo.Text = "Save Series Info";
            this.btnSeasonInfo.Click += new System.EventHandler(this.btnSeasonInfo_Click);
            // 
            // pbTVBanner
            // 
            this.pbTVBanner.Location = new System.Drawing.Point(11, 195);
            this.pbTVBanner.Name = "pbTVBanner";
            this.pbTVBanner.Size = new System.Drawing.Size(185, 101);
            this.pbTVBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTVBanner.TabIndex = 14;
            this.pbTVBanner.TabStop = false;
            // 
            // btnTVSaveThisOne
            // 
            this.btnTVSaveThisOne.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTVSaveThisOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTVSaveThisOne.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.btnTVSaveThisOne.Enabled = false;
            this.btnTVSaveThisOne.Location = new System.Drawing.Point(962, 6);
            this.btnTVSaveThisOne.Name = "btnTVSaveThisOne";
            this.btnTVSaveThisOne.Size = new System.Drawing.Size(92, 23);
            this.btnTVSaveThisOne.TabIndex = 12;
            this.btnTVSaveThisOne.Text = "Save This One";
            this.btnTVSaveThisOne.Click += new System.EventHandler(this.btnTVSaveThisOne_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(204, 37);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(842, 259);
            this.flowLayoutPanel2.TabIndex = 8;
            // 
            // pbTV
            // 
            this.pbTV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbTV.Location = new System.Drawing.Point(11, 9);
            this.pbTV.Name = "pbTV";
            this.pbTV.Size = new System.Drawing.Size(124, 179);
            this.pbTV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTV.TabIndex = 0;
            this.pbTV.TabStop = false;
            // 
            // tabTV
            // 
            this.tabTV.AttachedControl = this.tabControlPanel2;
            this.tabTV.Icon = ((System.Drawing.Icon)(resources.GetObject("tabTV.Icon")));
            this.tabTV.Name = "tabTV";
            this.tabTV.Text = "TV Series";
            this.tabTV.Visible = false;
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 25);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(1067, 638);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 4;
            this.tabControlPanel4.TabItem = this.tabItem1;
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel4;
            this.tabItem1.Icon = ((System.Drawing.Icon)(resources.GetObject("tabItem1.Icon")));
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "File Management";
            this.tabItem1.Visible = false;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Choose Directory with Movies";
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbProgress,
            this.sbStatus,
            this.toolStripDropDownButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1067, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sbProgress
            // 
            this.sbProgress.Name = "sbProgress";
            this.sbProgress.Size = new System.Drawing.Size(200, 16);
            this.sbProgress.Step = 1;
            // 
            // sbStatus
            // 
            this.sbStatus.Name = "sbStatus";
            this.sbStatus.Size = new System.Drawing.Size(782, 17);
            this.sbStatus.Spring = true;
            this.sbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmDetails,
            this.tsmTiles,
            this.toolStripSeparator1,
            this.tsm100,
            this.tsm75,
            this.tsm50,
            this.tsm25});
            this.toolStripDropDownButton1.Image = global::WDTVHubGen.Properties.Resources.zoom_in;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(68, 20);
            this.toolStripDropDownButton1.Text = "Zoom";
            // 
            // tsmDetails
            // 
            this.tsmDetails.Name = "tsmDetails";
            this.tsmDetails.Size = new System.Drawing.Size(109, 22);
            this.tsmDetails.Text = "Details";
            this.tsmDetails.Click += new System.EventHandler(this.tsmDetails_Click);
            // 
            // tsmTiles
            // 
            this.tsmTiles.Name = "tsmTiles";
            this.tsmTiles.Size = new System.Drawing.Size(109, 22);
            this.tsmTiles.Text = "Tiles";
            this.tsmTiles.Click += new System.EventHandler(this.tsmTiles_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(106, 6);
            // 
            // tsm100
            // 
            this.tsm100.Name = "tsm100";
            this.tsm100.Size = new System.Drawing.Size(109, 22);
            this.tsm100.Text = "100";
            this.tsm100.Click += new System.EventHandler(this.tsm100_Click);
            // 
            // tsm75
            // 
            this.tsm75.Name = "tsm75";
            this.tsm75.Size = new System.Drawing.Size(109, 22);
            this.tsm75.Text = "75";
            this.tsm75.Click += new System.EventHandler(this.tsm75_Click);
            // 
            // tsm50
            // 
            this.tsm50.Name = "tsm50";
            this.tsm50.Size = new System.Drawing.Size(109, 22);
            this.tsm50.Text = "50";
            this.tsm50.Click += new System.EventHandler(this.tsm50_Click);
            // 
            // tsm25
            // 
            this.tsm25.Name = "tsm25";
            this.tsm25.Size = new System.Drawing.Size(109, 22);
            this.tsm25.Text = "25";
            this.tsm25.Click += new System.EventHandler(this.tsm25_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 685);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMainForm";
            this.Text = "WDTV Live Hub Autogenerator v2.3.1";
            this.Load += new System.EventHandler(this.frmMainForm_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewMovie)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBannerBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewTV)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTVBanner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTV)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ButtonX btnPathBrowse;
        private System.Windows.Forms.Label label1;
        private ButtonX btnProcess;
        private ButtonX btnGetFiles;
       // private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.TabControl tabControl1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevComponents.DotNetBar.TabItem tabMovies;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabTV;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox chkShowUnprocessed;
        private ButtonX btnSaveThisOne;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ButtonX btnNextImage;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkGroupByProcessedTV;
        private System.Windows.Forms.TextBox txtSeriesPath;
        private ButtonX btnChangePath;
        private System.Windows.Forms.Label label2;
        private ButtonX btnGetShows;
        private ButtonX btnProcessAllTV;
        private ButtonX btnTVSaveThisOne;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.PictureBox pbTV;
        private System.Windows.Forms.PictureBox pbTVBanner;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameFileToolStripMenuItem;
        private System.Windows.Forms.TextBox txtRenameFile;
        private ButtonX btnRenameFile;
        private ButtonX btnCancelRename;
        private ButtonX btnSeasonInfo;
        private System.Windows.Forms.ToolStripMenuItem lookupInformationToolStripMenuItem;
        private BrightIdeasSoftware.ObjectListView objectListViewMovie;
        private BrightIdeasSoftware.OLVColumn olvcTitle;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvcXMLExists;
        private BrightIdeasSoftware.OLVColumn olvcThumbPresent;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvcDescription;
        private BrightIdeasSoftware.OLVColumn olvColumn7;
        private BrightIdeasSoftware.ImageRenderer imageRenderer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar sbProgress;
        private System.Windows.Forms.ToolStripStatusLabel sbStatus;
        private DevComponents.DotNetBar.Controls.ReflectionLabel lblMovieTitle;
        private ButtonX btnShowPanelDetail;
        private BrightIdeasSoftware.ObjectListView objectListViewTV;
        private BrightIdeasSoftware.OLVColumn olvcThumbTV;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvcXMLPresentTV;
        private BrightIdeasSoftware.OLVColumn olvcThumbPresentTV;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private BrightIdeasSoftware.OLVColumn olvcSeason;
        private BrightIdeasSoftware.OLVColumn olvcEpisode;
        private System.Windows.Forms.ToolStripMenuItem aggressiveLookupInformationToolStripMenuItem;
        private DevComponents.DotNetBar.Controls.ReflectionLabel reflectionLabel2;
        private ButtonX btnTVNextImage;
        private ButtonX btnShowTVDetailPanel;
        private ButtonX btnCancelRenameTV;
        private ButtonX btnRenameTV;
        private System.Windows.Forms.TextBox txtRenameTV;
        private BalloonTip balloonTip1;
        private TabControlPanel tabControlPanel3;
        private TabItem tabOptions;
        private PropertyGridEx.PropertyGridEx propertiesGrid;
        private System.Windows.Forms.ToolStripMenuItem removeExistingXMLThumbToolStripMenuItem;
        private BrightIdeasSoftware.OLVColumn olvSubsPresent;
        private BrightIdeasSoftware.OLVColumn olvcSubsPresent;
        private ButtonX btnShowMoreSeries;
        private ButtonX btnSelectAlternateMovies;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pbBannerBox;
        private System.Windows.Forms.ToolStripMenuItem openFolderLocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsmDetails;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsm100;
        private System.Windows.Forms.ToolStripMenuItem tsm75;
        private System.Windows.Forms.ToolStripMenuItem tsm50;
        private System.Windows.Forms.ToolStripMenuItem tsm25;
        private System.Windows.Forms.ToolStripMenuItem tsmTiles;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private System.Windows.Forms.ImageList imageList1;
        private BrightIdeasSoftware.OLVColumn olvcDate;
        private BrightIdeasSoftware.OLVColumn olvcProcessedDate;
        private BrightIdeasSoftware.OLVColumn olvsSeasons;
        private BrightIdeasSoftware.OLVColumn olvcEpisodes;
        private BrightIdeasSoftware.OLVColumn olvcVideoType;
        private readonly ICacheProvider m_cacheProvider;
        private ButtonX btnToggleHidden;
        private System.Windows.Forms.ComboBox cboPath;
        private System.Windows.Forms.ToolStripMenuItem manuallySearchForTitleToolStripMenuItem;
        private ButtonX btnSeriesInfo;
        private System.Windows.Forms.ToolStripMenuItem moveToAnotherFolderToolStripMenuItem;
        private TabControlPanel tabControlPanel4;
        private TabItem tabItem1;
        private System.Windows.Forms.ToolStripMenuItem renameBackToOriginalNameToolStripMenuItem;
        private System.Windows.Forms.Button btnUserImage;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

