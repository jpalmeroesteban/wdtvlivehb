﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TheMovieDB
{
    [XmlType("image")]
    public class TmdbImage
    {
        //private TmdbImageSize _size;
        
        [XmlAttribute("type")]
        public TmdbImageType Type { get; set; }

        [XmlAttribute("size")]
        public string Size { get; set; }
       /* public TmdbImageSize Size
        {
            get { return _size; } 
            set
            {
                if (Enum.IsDefined(typeof(TmdbImageSize), value) == true)
                _size = value;
                else _size = TmdbImageSize.other;
            }
        } */

        

        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }
    }
}
