﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WDTVHubGen.LoggingService;

namespace WDTVHubGen
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoggingService.CLogger.WriteLog(ELogLevel.DEBUG, "Starting Up");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CLogger.WriteLog(ELogLevel.DEBUG, "About to Launch Form");
            //Application.Run(new frmHubGenMain());
            Application.Run(new frmMainForm());
        }
    }
}
